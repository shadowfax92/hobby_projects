#to test - run this and use 'tesseract final_output.png out' to see the conversion
from PIL import Image 
from pyocr import pyocr
import ImageEnhance
from urllib import urlretrieve
import sys 
import os
import re
import requests
import json
#defining global session
session=0

def get(link):
    #get_response = requests.get('https://pizzaonline.dominos.co.in/slot-machine/')
    #session=requests.session()
    #print session
    urlretrieve(link,'temp.png')
    return

def send_captcha(cap_val):
    global session
    #session = requests.Session()
    headers = {'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8', 'X-Requested-With': 'XMLHttpRequest'}
    data = {'cap': cap_val}
    response = None
    request = session.post('https://pizzaonline.dominos.co.in/slot-machine/captcha-action.php',data=data, headers=headers)
    captcha_valid_response=str(request.text)
    return captcha_valid_response

def fetch_offer():
    headers = {'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8', 'X-Requested-With': 'XMLHttpRequest'}
    data = {'session_id': str(session)}
    response = None
    i=1
    while i<3:
        request = session.post('https://pizzaonline.dominos.co.in/slot-machine/process-slot.php', data=data, headers=headers)
        response = json.loads(request.text)
        if int(response['status']) == 1 and str(response['slot_result']) != '4':
            print_str='coupon code: '+str(response['unique_coupon'])+' coupon description: '+str(response['coupon_description'])
            i=int(response['slot_attempts'])
            print print_str
            append_output(print_str)
        #print str(request.text)
        #append_output(str(request.text))
        #i+=1
        print 'i:'+str(i)
    #print str(response)
    return

def append_output(response):
    file=open("output_of_query.txt","a")
    file.write(response)
    file.write('\n')
    file.close()
    return

def get_captcha():
    global session
    #get session id   
    get_response = requests.get('https://pizzaonline.dominos.co.in/slot-machine/')
    session=requests.session()
    #get the captcha
    get('https://pizzaonline.dominos.co.in/slot-machine/captcha.php');
    im = Image.open("temp.png")

    #crop image
    #A(12.5, 7.5) to B(70.5, 21.5).
    im_tmp=im.crop((12,7,70,21)).save("temp_nik.png")
    imgx = Image.open('temp_nik.png')
    imgx = imgx.convert("RGBA")
    pix = imgx.load()
    for y in xrange(imgx.size[1]):
        for x in xrange(imgx.size[0]):
            if pix[x, y] != (0, 0, 0, 255):
                pix[x, y] = (255, 255, 255, 255)
    imgx.save("final_output.png", "PNG")
    #perform text recong using tesseract library
    command_tesseract='tesseract final_output.png out'
    os.system(command_tesseract)
    fh = open('out.txt','rU')
    output_tmp=fh.read().rstrip()
    output=str(re.sub('P','f',output_tmp))
    print output_tmp,output
    res = send_captcha(output)
    if res=='1':
        print 'Successful'
    else:
        print 'Failed'
    fetch_offer()

#main
for i in range(1000):
    print 'Request No: '+str(i)
    get_captcha()
