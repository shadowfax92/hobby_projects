#########################################################################
# Author: Nikhil Venkat Sonti
# Description: 	Takes the url address and tries to download all mp4 files
#				using wget.
# To Add:
#		- Suppor to specify Directory path
#		- Support to specify filename or take file name from URL
#		- wget on multiple threads to reduce the download time
#########################################################################


import urlparse
import urllib
import sys
import re
import os

def print_debug(name,value):
	print str(name)+" : "+ str(value)
	return

def download_videos(urls):
	for url in urls:
		cmd = 'wget '+url
		print_debug('wget command',cmd)
		os.system(cmd)
		sys.exit(0)
	return


#read the input url
print 'Enter the site address:'
url=sys.stdin.readline().rstrip()
#url='https://class.coursera.org/nlp/lecture/index'
print_debug('input url',url)

#get the html for the url
try:
	htmltext = urllib.urlopen(url).read()
except:
	print_debug('Invalid URL',url)
	sys.exit(0)

#print_debug('HTML Text',htmltext)
video_urls = re.findall(r'https://.*?mp4.*',htmltext)
print_debug('video urls',video_urls)
download_videos(video_urls)

